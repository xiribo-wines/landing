const defaultTheme = require("tailwindcss/defaultTheme");

const theme = {
  ...defaultTheme,
  extend: {
    colors: {
      primary: "#50e3c2"
    },
    fill: theme => ({
      primary: theme("colors.pimary"),
      transparent: "transparent"
    }),
    stroke: theme => ({
      primary: theme("colors.pimary"),
      transparent: "transparent"
    })
  }
};

module.exports = {
  theme,
  variants: ["responsive", "hover", "focus", "disabled"],
  plugins: []
};
