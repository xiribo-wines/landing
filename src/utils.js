export const delay = t => new Promise(resolve => setTimeout(resolve, t));
export function scrollToTarget(e) {
    document.querySelector(this.getAttribute("href")).scrollIntoView({
        behavior: "smooth"
    })
}
